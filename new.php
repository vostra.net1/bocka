<?php error_reporting(0); ?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <title>Ideální model vrhu koulí - nový vrh</title>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta name="robots" content="noindex, nofollow">
  <meta name="googlebot" content="noindex, nofollow">
  <meta name="viewport" content="width=device-width, initial-scale=1">


  <script
    type="text/javascript"
    src="/js/lib/dummy.js"
    
  ></script>

    <link rel="stylesheet" type="text/css" href="/css/result-light.css">

      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
      <script type="text/javascript" src="//code.jquery.com/jquery-3.3.1.slim.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
      <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
      <script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

  <style id="compiled-css" type="text/css">

body {
  min-height: 100vh;

  background-color: #00FF00;
  background-image: linear-gradient(147deg, #00FF00 0%, #003300 100%);
}

  </style>

</head>
<body>
<div class="container py-5">
  <header class="text-center text-white">
    <h1 class="display-4">Ideální model vrhu koulí - nový vrh</h1>
    <p class="lead mb-0">Thea Vostrá, Gymnázium Botičská</p>
    <p class="lead mb-0"><br/><a href="index.php" class="text-white"><- Zpět na celý seznam</a></p>
  </header>
    <div class="row py-5">
    <div class="col-lg-10 mx-auto">
      <div class="card rounded shadow border-0">
        <div class="card-body p-5 bg-white rounded">
          <div class="table-responsive">
  <form action="detail.php" method="get">
  <div class="form-group">
    <label for="PocRychVrhu">Počáteční rychlost vrhu</label>
    <input name="V0" class="form-control" id="PocRychVrhu">
    <small id="PocRychVrhuInfo" class="form-text text-muted">Zadejte hodnotu v m/s v rozmezí od 0 do 12.</small>
 </div>
  <div class="form-group">
    <label for="VysBonMerVrhu">Výška bonusu měřeného vrhu</label>
    <input name="Y0" class="form-control" id="VysBonMerVrhu">
    <small id="VysBonMerVrhuInfo" class="form-text text-muted">Zadejte hodnotu v m v rozmezí od 0 do 5.</small>
 </div>
  <div class="form-group">
    <label for="UhelMerVrhu">Úhel měřeného vrhu</label>
    <input name="A0" class="form-control" id="UhelMerVrhu">
    <small id="UhelMerVrhuInfo" class="form-text text-muted">Zadejte hodnotu ve stupních v rozmezí od 0 do 90.</small>
 </div>

  <button type="submit" class="btn btn-primary">Modelovat nový vrh</button>
</form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <footer class="text-center text-white">
   <p class="font-italic">Používá technologie Bootstrap 4 a Bootstrap 4 Datatables</p>
  </footer>
</div>

</body>
</html> 
