<?php
    /* Vstupní parametry */
    $g0 = 9.80665;
    $v0 = $_GET['V0'];
    $y0 = $_GET['Y0'];
    $a0 = $_GET['A0'];
    $a0i = $_GET['A0I'];
    $xmax = 13;
    $ymax = 5;

    /* Hraniční parametry obrázku */
    $width = 1000;
    $height = 500;
    $xx = 100;
    $yy = 100;
    $xxmax = $width - 2 * $xx;
    $yymax = $height - 2 * $yy;
    $mx = $xxmax / $xmax;
    $my = $yymax / $ymax;

    /* Založení obrázku */
    $png = imagecreatetruecolor($width, $height);
    imagesavealpha($png, true);

    $trans_colour = imagecolorallocatealpha($png, 0, 0, 0, 127);
    imagefill($png, 0, 0, $trans_colour);
   
    /* Nastavení barev */
    $red = imagecolorallocate($png, 255, 0, 0);
    $green = imagecolorallocate($png, 0, 255, 0);
    $black = imagecolorallocate($png, 0, 0, 0);
    $white = imagecolorallocate($png, 255, 255, 255);
    
    /* Rámeček */
    imagefilledellipse($png, 400, 300, 400, 300, $red);
    imagefilledrectangle($png, 0, 0, $width, $height, $black);
    imagefilledrectangle($png, 1, 1, $width-2, $height-2, $white);
  

    /* Osy */
    imageline($png, $width-$xx, $yy, $width-$xx, $height-$yy, $black);
    imageline($png, $width-$xx-10, $yy+10, $width-$xx, $yy, $black);
    imageline($png, $width-$xx+10, $yy+10, $width-$xx, $yy, $black);
    imageline($png, $width-$xx, $height-$yy, $xx, $height-$yy, $black);
    imageline($png, $xx+10, $height-$yy-10, $xx, $height-$yy, $black);
    imageline($png, $xx+10, $height-$yy+10, $xx, $height-$yy, $black);

    $font = dirname(__FILE__).'/roboto.ttf';
    imagettftext($png, 12, 0, $width-$xx+25, $height-$yy+25, $black, $font, "0");
    imagettftext($png, 12, 0, $xx+25, $yy, $red, $font, "měřený hod");
    imagettftext($png, 12, 0, $xx+25, $yy+25, $green, $font, "ideální hod");
    imagettftext($png, 20, 0, $xx-40, $height-$yy+10, $black, $font, "x");
    imagettftext($png, 20, 0, $width-$xx-5, $yy-30, $black, $font, "y");

    /* Jednotky na x */
    for ($i = 1; $i < $xmax; $i = $i + 1) {
        imageline($png, $width-$xx-$i*$mx, $height-$yy-5, $width-$xx-$i*$mx, $height-$yy+5, $black);
        imagettftext($png, 12, 0, $width-$xx-$i*$mx-5, $height-$yy+25, $black, $font, $i);
    }

    /* Jednotky na y */
    for ($i = 1; $i < $ymax; $i = $i + 1) {
        imageline($png, $width-$xx-5, $height-$yy-$i*$my, $width-$xx+5, $height-$yy-$i*$my, $black);
        imagettftext($png, 12, 0, $width-$xx+25, $height-$yy-$i*$my+5, $black, $font, $i);
    }

    /* Kreslení měřeného hodu */
    $td = ($v0 * sin(deg2rad($a0)) + sqrt(($v0 * $v0 * sin(deg2rad($a0)) * sin(deg2rad($a0))) + (2 * $y0 * $g0))) / $g0;
    $delta = $td / $xxmax;
    for ($t = 0; $t <= $td; $t = $t + $delta) {
        $x = $v0 * $t * cos(deg2rad($a0));
        $y = $v0 * $t * sin(deg2rad($a0)) - 0.5 * $g0 * $t * $t;
        imageline($png, $width-$xx-$x*$mx, $height-$yy-$y0*$my-$y*$my, $width-$xx-$x*$mx, $height-$yy-$y0*$my-$y*$my, $red);
    }
    /* Kreslení ideálního hodu */
    $td = ($v0 * sin(deg2rad($a0i)) + sqrt(($v0 * $v0 * sin(deg2rad($a0i)) * sin(deg2rad($a0i))) + (2 * $y0 * $g0))) / $g0;
    $delta = $td / $xxmax;
    for ($t = 0; $t <= $td; $t = $t + $delta) {
        $x = $v0 * $t * cos(deg2rad($a0i));
        $y = $v0 * $t * sin(deg2rad($a0i)) - 0.5 * $g0 * $t * $t;
        imageline($png, $width-$xx-$x*$mx, $height-$yy-$y0*$my-$y*$my, $width-$xx-$x*$mx, $height-$yy-$y0*$my-$y*$my, $green);
    }
  
    /* Poslání obrázku */
    header("Content-type: image/png");
    imagepng($png);
?>
