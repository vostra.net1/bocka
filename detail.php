<?php error_reporting(0); ?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <title>Ideální model vrhu koulí - detail</title>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta name="robots" content="noindex, nofollow">
  <meta name="googlebot" content="noindex, nofollow">
  <meta name="viewport" content="width=device-width, initial-scale=1">


  <script
    type="text/javascript"
    src="/js/lib/dummy.js"
    
  ></script>

    <link rel="stylesheet" type="text/css" href="/css/result-light.css">

      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
      <script type="text/javascript" src="//code.jquery.com/jquery-3.3.1.slim.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
      <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
      <script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

  <style id="compiled-css" type="text/css">

body {
  min-height: 100vh;

  background-color: #00FF00;
  background-image: linear-gradient(147deg, #00FF00 0%, #003300 100%);
}

  </style>

</head>
<body>
<div class="container py-5">
  <header class="text-center text-white">
    <h1 class="display-4">Ideální model vrhu koulí - detail</h1>
    <p class="lead mb-0">Thea Vostrá, Gymnázium Botičská</p>
    <p class="lead mb-0"><br/><a href="index.php" class="text-white"><- Zpět na celý seznam</a></p>
    <?php
    (float)$g0 = 9.80665;
    (float)$v0 = $_GET['V0'];
    (float)$y0 = $_GET['Y0'];
    (float)$a0 = $_GET['A0'];
    (float)$a0i = $_GET['A0I'];
    //if ($a0i==0) (float)$a0i = rad2deg(asin((sqrt(2) * $v0) / (2 * sqrt($v0 * $v0 * $g0 * $y0))));
    if ($a0i==0) (float)$a0i = 39;

    echo "<div align=\"center\"><br/>";
    echo "<img src=\"draw.php?V0=".(float)$v0."&Y0=".(float)$y0."&A0=".(float)$a0."&A0I=".(float)$a0i."\" alt=\"vrh\" style=\"width:920px;height:500px;\">";
    echo "</div>";
?>
  </header>
  <div class="row py-5">
    <div class="col-lg-10 mx-auto">
      <div class="card rounded shadow border-0">
        <div class="card-body p-5 bg-white rounded">
          <div class="table-responsive">
            <table id="example" style="width:1000px" class="table table-striped table-bordered">
              <thead>
              <tr>
                <th>Parametr</th>
                <th>Hodnota</th>
                <th>Jednotka</th>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td>Počáteční rychlost vrhu</td>
                <td><strong><?php echo number_format($v0,4,","," ")." m/s"; ?></strong></td>
                <td>[m/s]</td>
              </tr>
              <tr>
                <td>Výška bonusu měřeného vrhu</td>
                <td><?php echo number_format($y0,2,","," ")." m"; ?></td>
                <td>[m]</td>
              </tr>
              <tr>
                <td>Úhel měřeného vrhu</td>
                <td><?php echo number_format($a0,2,","," ")." stupňů"; ?></td>
                <td>[stupňů]</td>
              </tr>
              <tr>
                <td>Čas k dosažení vrcholu měřeného vrhu</td>
                <td>
                  <?php
                    (float)$tv = $v0 * sin(deg2rad($a0)) / $g0;
                    echo number_format($tv,2,","," ")." s";
                  ?>
                </td>
                <td>[s]</td>
              </tr>
              <tr>
                <td>Výška vrcholu měřeného vrhu</td>
                <td>
                  <?php
                    (float)$yv = $y0 + ($v0 * $v0 * sin(deg2rad($a0)) * sin(deg2rad($a0)) / (2 * $g0));
                    echo number_format($yv,2,","," ")." m";
                  ?>
                </td>
                <td>[m]</td>
              </tr>
              <tr>
                <td>Celkový čas měřeného vrhu</td>
                <td>
                  <?php
                    (float)$td = ($v0 * sin(deg2rad($a0)) + sqrt($v0 * $v0 * sin(deg2rad($a0)) * sin(deg2rad($a0)) + (2 * $y0 * $g0 )) ) / $g0;
                    echo number_format($td,2,","," ")." s";
                  ?>
                </td>
                <td>[s]</td>
              </tr>
              <tr>
                <td>Délka měřeného vrhu</td>
                <td><strong>
                  <?php
                    (float)$xd = (($v0 * $v0 * sin(deg2rad(2 * $a0))) + sqrt(($v0 * $v0 * $v0 * $v0 * sin(deg2rad(2 * $a0)) * sin(deg2rad(2 * $a0))) + (8 * $y0 * $g0 * $v0 * $v0 * cos(deg2rad($a0)) * cos(deg2rad($a0))))) / (2 * $g0);
                    echo number_format($xd,2,","," ")." m";
                  ?>
                </strong> (červeně)</td>
                <td>[m]</td>
              </tr>
              <tr>
                <td>Úhel ideálního vrhu</td>
                <td>
                  <?php
                    //(float)$a0i = rad2deg(asin((sqrt(2) * $v0) / (2 * sqrt($v0 * $v0 * $g0 * $y0))));
                    echo number_format($a0i,2,","," ")." stupňů";
                  ?>
                </td>
                <td>[stupňů]</td>
              </tr>
              <tr>
                <td>Čas k dosažení vrcholu ideálního vrhu</td>
                <td>
                  <?php
                    (float)$tvi = $v0 * sin(deg2rad($a0i)) / $g0;
                    echo number_format($tvi,2,","," ")." s";
                  ?>
                </td>
                <td>[s]</td>
              </tr>
              <tr>
                <td>Výška vrcholu ideálního vrhu</td>
                <td>
                  <?php
                    (float)$yvi = $y0 + ($v0 * $v0 * sin(deg2rad($a0i)) * sin(deg2rad($a0i)) / (2 * $g0));
                    echo number_format($yvi,2,","," ")." m";
                  ?>
                </td>
                <td>[m]</td>
              </tr>
              <tr>
                <td>Celkový čas ideálního vrhu</td>
                <td>
                  <?php
                    (float)$tdi = ($v0 * sin(deg2rad($a0i)) + sqrt($v0 * $v0 * sin(deg2rad($a0i)) * sin(deg2rad($a0i)) + (2 * $y0 * $g0 )) ) / $g0;
                    echo number_format($tdi,2,","," ")." s";
                  ?>
                </td>
                <td>[s]</td>
              </tr>
              <tr>
                <td>Délka ideálního vrhu</td>
                <td><strong>
                  <?php
                    (float)$xdi = (($v0 * $v0 * sin(deg2rad(2 * $a0i))) + sqrt(($v0 * $v0 * $v0 * $v0 * sin(deg2rad(2 * $a0i)) * sin(deg2rad(2 * $a0i))) + (8 * $y0 * $g0 * $v0 * $v0 * cos(deg2rad($a0i)) * cos(deg2rad($a0i))))) / (2 * $g0);
                    echo number_format($xdi,2,","," ")." m";
                  ?>
                </strong> (zeleně)</td>
                <td>[m]</td>
              </tr>
              <tr>
                <td>Rozdíl mezi měřeným a ideálním vrhem</td>
                <td><?php echo number_format(($xdi - $xd),2,","," ")." m"; ?></td>
                <td>[m]</td>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <footer class="text-center text-white">
   <p class="font-italic">Používá technologie Bootstrap 4 a Bootstrap 4 Datatables</p>
  </footer>
</div>

</body>
</html> 
