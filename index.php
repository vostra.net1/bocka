<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <title>Ideální model vrhu koulí</title>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta name="robots" content="noindex, nofollow">
  <meta name="googlebot" content="noindex, nofollow">
  <meta name="viewport" content="width=device-width, initial-scale=1">


  <script
    type="text/javascript"
    src="/js/lib/dummy.js"
    
  ></script>

    <link rel="stylesheet" type="text/css" href="/css/result-light.css">

      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
      <script type="text/javascript" src="//code.jquery.com/jquery-3.3.1.slim.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
      <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
      <script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

  <style id="compiled-css" type="text/css">

body {
  min-height: 100vh;

  background-color: #00FF00;
  background-image: linear-gradient(147deg, #00FF00 0%, #003300 100%);
}

  </style>

  <script id="insert"></script>


    <script src="/js/stringify.js?2e1a08e11df17968898e3ff29f7e6486935607e9" charset="utf-8"></script>
    <script>
      const customConsole = (w) => {
        const pushToConsole = (payload, type) => {
          w.parent.postMessage({
            console: {
              payload: stringify(payload),
              type:    type
            }
          }, "*")
        }

        w.onerror = (message, url, line, column) => {
          // the line needs to correspond with the editor panel
          // unfortunately this number needs to be altered every time this view is changed
          line = line - 70
          if (line < 0){
            pushToConsole(message, "error")
          } else {
            pushToConsole(`[${line}:${column}] ${message}`, "error")
          }
        }

        let console = (function(systemConsole){
          return {
            log: function(){
              let args = Array.from(arguments)
              pushToConsole(args, "log")
              systemConsole.log.apply(this, args)
            },
            info: function(){
              let args = Array.from(arguments)
              pushToConsole(args, "info")
              systemConsole.info.apply(this, args)
            },
            warn: function(){
              let args = Array.from(arguments)
              pushToConsole(args, "warn")
              systemConsole.warn.apply(this, args)
            },
            error: function(){
              let args = Array.from(arguments)
              pushToConsole(args, "error")
              systemConsole.error.apply(this, args)
            },
            system: function(arg){
              pushToConsole(arg, "system")
            },
            clear: function(){
              systemConsole.clear.apply(this, {})
            },
            time: function(){
              let args = Array.from(arguments)
              systemConsole.time.apply(this, args)
            },
            assert: function(assertion, label){
              if (!assertion){
                pushToConsole(label, "log")
              }

              let args = Array.from(arguments)
              systemConsole.assert.apply(this, args)
            }
          }
        }(window.console))

        window.console = { ...window.console, ...console }

        console.system("Running fiddle")
      }

      if (window.parent){
        customConsole(window)
      }
    </script>
</head>
<body>
<div class="container py-5">
  <header class="text-center text-white">
    <h1 class="display-4">Ideální model vrhu koulí</h1>
    <p class="lead mb-0">Thea Vostrá, Gymnázium Botičská</p>
    <p class="lead mb-0"><br/><a href="new.php" class="text-white">Modelovat nový vrh</a></p>
  </header>
  <div class="row py-5">
    <div class="col-lg-10 mx-auto">
      <div class="card rounded shadow border-0">
        <div class="card-body p-5 bg-white rounded">
          <div class="table-responsive">
            <table id="example" style="width:1000px" class="table table-striped table-bordered">
              <thead>
              <tr>
                <th>#</th>
                <th>datum</th>
                <th>jméno</th>
                <th>V0 [m/s]</th>
                <th>Y0 [m]</th>
                <th>A0 [deg]</th>
                <th>TD [s]</th>
                <th>XD [m]</th>
              </tr>
              </thead>
              <tbody>
                <?php
                  $string = file_get_contents("./data.json");
                  $json_a = json_decode($string, true);
                  for($idx = 0; $idx < count($json_a); $idx++){
                    $obj = (Array)$json_a[$idx];
                    echo "<tr>";
                    echo "<td><a href=\"detail.php?V0=".str_replace(",",".",$obj["V0"])."&Y0=".str_replace(",",".",$obj["Y0"])."&A0=".str_replace(",",".",$obj["A0"])."&A0I=".str_replace(",",".",$obj["IA0"])."\">".$obj["line"]."</a></td>";
                    echo "<td>".$obj["date"]."</td>";
                    echo "<td>".$obj["name"]."</td>";
                    echo "<td>".number_format(str_replace(",",".",$obj["V0"]),4,","," ")."</td>";
                    echo "<td>".number_format(str_replace(",",".",$obj["Y0"]),2,","," ")."</td>";
                    echo "<td>".number_format(str_replace(",",".",$obj["A0"]),2,","," ")."</td>";
                    echo "<td>".number_format(str_replace(",",".",$obj["TD"]),2,","," ")."</td>";
                    echo "<td>".number_format(str_replace(",",".",$obj["XD"]),2,","," ")."</td>";
                    echo "</tr>";
                  }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <footer class="text-center text-white">
   <p class="font-italic">Používá technologie Bootstrap 4 a Bootstrap 4 Datatables</p>
  </footer>
</div>


    <script type="text/javascript">//<![CDATA[


$(function() {
  $(document).ready(function() {
    $('#example').DataTable();
  });
});



  //]]></script>

  <script>
    // tell the embed parent frame the height of the content
    if (window.parent && window.parent.parent){
      window.parent.parent.postMessage(["resultsFrame", {
        height: document.body.getBoundingClientRect().height,
        slug: "j1xfcah5"
      }], "*")
    }

    // always overwrite window.name, in case users try to set it manually
    window.name = "result"
  </script>

    <script>
      let allLines = []

      window.addEventListener("message", (message) => {
        if (message.data.console){
          let insert = document.querySelector("#insert")
          allLines.push(message.data.console.payload)
          insert.innerHTML = allLines.join(";\r")

          let result = eval.call(null, message.data.console.payload)
          if (result !== undefined){
            console.log(result)
          }
        }
      })
    </script>

</body>
</html>
