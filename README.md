# Ideální model vrhu koulí, Thea Vostrá, Gymnázium Botičská

# Wikipedie

Vrh koulí je atletická disciplína s velmi starobylými kořeny. Cílem je vrhnout těžkou kouli co nejdále, atleti nesmějí koulí házet. Aby vyvinuli co nejvíce pohybové energie, postaví se do zadní poloviny vrhačského kruhu o průměru 2,135 m zády k výseči, kam dopadá koule. Přiloží nářadí ke krku a obrátí se čelem k výseči.

Někteří atleti při vrhu používají techniku podobnou diskařské otočce. Pokud ji zvládnou, koule většinou letí dál než při klasické technice „sunu“. Koule, kterou vrhají muži, váží 7,26 kg a má průměr 13 cm, ženy používají lehčí a menší kouli, ta váží 4 kg a má průměr 11 cm. 

# Hlavní obrazovka

<img src="/main_page.png" width="600" />

# Obrazovka detailu vrhu

<img src="/detail_page.png" width="600" />

# Modelace nového vrhu

<img src="/new_page.png" width="600" />
